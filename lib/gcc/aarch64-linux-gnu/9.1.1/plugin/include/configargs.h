/* Generated automatically. */
static const char configuration_arguments[] = "../gcc/configure --enable-languages=c,c++ --target=aarch64-linux-gnu --prefix=/mnt/data1/GitHub/build-tools-gcc/aarch64-linux-gnu --disable-multilib --disable-werror CFLAGS='-g0 -O2 -fstack-protector-strong' CXXFLAGS='-g0 -O2 -fstack-protector-strong'";
static const char thread_model[] = "posix";

static const struct {
  const char *name, *value;
} configure_default_options[] = { { NULL, NULL} };
